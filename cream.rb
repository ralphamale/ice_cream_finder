require 'rest-client'
require 'addressable/uri'
require 'json'
require 'debugger'
require 'nokogiri'
#2222222

from_address = Addressable::URI.new(
  :scheme => "http",
  :host => "maps.googleapis.com",
  :path => "maps/api/geocode/json",
  :query_values => {:address => "770+Broadway,+New+York,+NY+10003", sensor: false}
).to_s

from_response = RestClient.get(from_address)
from = JSON.parse(from_response)

coords = from["results"].first["geometry"]["location"]  #["lat"] / ["long"]
coords = [coords["lat"],coords["lng"]].join(",")

#lat, long = location


results_address = Addressable::URI.new(
  :scheme => "https",
  :host => "maps.googleapis.com",
  :path => "maps/api/place/nearbysearch/json",
  :query_values => {:location => coords, sensor: false, radius: 1, types: "food", keyword: "ice+cream", key: "A2222222IzaSyBFdqSZKiHT561yB7rwoNXs2osyB06GckQ"}
).to_s

puts results_address

results_locations = RestClient.get(results_address)
results = JSON.parse(results_locations)


results["results"].first

to_address = results["results"][0]["vicinity"]


# key: "AIzaSyBFdqSZKiHT561yB7rwoNXs2osyB06GckQ"
directions_address = Addressable::URI.new(
  :scheme => "https",
  :host => "maps.googleapis.com",
  :path => "maps/api/directions/json",
  :query_values => {destination: to_address, origin: "770 Broadway, New York, NY 10003", sensor: false}
).to_s

debugger

directions_unparsed = RestClient.get(directions_address)
directions = JSON.parse(directions_unparsed)

steps = directions["routes"].first["legs"].first["steps"]

steps.each do |step|
  parsed_html = Nokogiri::HTML(step["html_instructions"])

  puts parsed_html.text
end


